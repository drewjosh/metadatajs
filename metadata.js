import request from 'request';
import cheerio from 'cheerio';
import clipboard from 'clipboardy';

// specify the URIs of the sites which contain the codes
const uriClassCodes = 'https://fhir.ch/ig/ch-epr-term/ValueSet-DocumentEntry.classCode.html#logical-definition-cld';
const uriTypeCodes = 'http://build.fhir.org/ig/hl7ch/ch-epr-term/ValueSet-DocumentEntry.typeCode.html#logical-definition-cld';
const uriAllergyCodes = 'http://fhir.ch/ig/ch-allergyintolerance/ValueSet-CHAllergyIntoleranceValueSet.html';
const uriAllergyReactionManifestation = 'http://fhir.ch/ig/ch-allergyintolerance/ValueSet-CHAllergyIntoleranceReactionManifestationValueSet.html';
const uriAllergyReactionSubstance = 'https://fhir.ch/ig/ch-allergyintolerance/ValueSet-CHAllergyIntoleranceReactionSubstanceValueSet.html';
const uriBodySite = 'https://hl7.org/fhir/R4/valueset-body-site.html';
const uriRouteCodes = 'https://hl7.org/fhir/R4/valueset-route-codes.html';

const args = process.argv.slice(1);
const uri = getURI(args);

/*
 * Connects to the specified URI and reads codes and print in specific format.
 */
request(uri, (error, response, html) => {
  // empty data array for results
  var data = [];
  if (response && response.statusCode === 200) {
    // define the loaded DOM as $ for jQuery syntax
    var $ = cheerio.load(html);

    // get first table because it containts the language displays
    var codesTable = $('table.codes').first(); // depending on the site there have to be a different selector, e.g. table.none
    var codeEntries = [];
    var codeTypeEntries = [];

    // true when want to log found type in the end
    var showTypes = false;

    // iterate over table rows which each represents on code
    $(codesTable)
      .find('tr')
      .each((i, row) => {
        if (i == 0) {
          // skip first because it's title of columns
          return;
        }
        var codeEntry = {};
        if (uri == uriBodySite || uri == uriRouteCodes) {
          // we use normal code structure
          codeEntry = {
            system: 'http://snomed.info/sct',
            code: '',
            display: ''
          };
        } else {
          codeEntry = {
            defaultCoding: {
              system: 'http://snomed.info/sct',
              code: '',
              display: ''
            },
            languageDisplays: {
              en: '',
              de: '',
              fr: '',
              it: '',
              rm: ''
            }
          };
        }

        // iterate over columns each representing one language in following order:
        // SNOMED Code, de, en, fr, it, rm
        $(row)
          .find('td')
          .each((j, column) => {
            var content = $(column).text();
            content = cleanString(content);

            // sadly they have different orders in the tables
            if (uri == uriAllergyCodes || uri == uriAllergyReactionSubstance) {
              showTypes = true;

              switch (j) {
                case 0:
                  codeEntry.defaultCoding.code = content;
                  break;
                case 1:
                  // type of concept
                  var type;
                  if (content == 'Mesalazine adverse reaction') {
                    // missing type in table
                    // source: https://browser.ihtsdotools.org/?perspective=full&conceptId1=292118005&edition=MAIN/2022-05-31&release=&languages=en
                    type = 'disorder';
                  } else if (content == 'European hornet venom') {
                    // missing type in table
                    // source: https://browser.ihtsdotools.org/?perspective=full&conceptId1=260196009&edition=MAIN/2022-05-31&release=&languages=en
                    type = 'substance';
                  } else if (content.indexOf(')') == -1) {
                    // some entries have missing ")"
                    type = content.substring(content.lastIndexOf('(') + 1, content.length);
                  } else {
                    type = content.substring(content.lastIndexOf('(') + 1, content.lastIndexOf(')'));
                  }
                  codeEntry.type = type;

                  // add to array if no already exist
                  if (codeTypeEntries.indexOf(type) == -1) {
                    codeTypeEntries.push(type);
                  }

                  break;
                case 2:
                  codeEntry.defaultCoding.display = content;
                  codeEntry.languageDisplays.en = content;
                  break;
                case 3:
                  codeEntry.languageDisplays.fr = content;
                  break;
                case 4:
                  codeEntry.languageDisplays.de = content;
                  break;
                case 5:
                  codeEntry.languageDisplays.it = content;
                  break;
                default:
                  console.warn('Cannot handle other column, j:' + j);
                  break;
              }
              // no translation for romantsch
              codeEntry.languageDisplays.rm = '?';
            } else if (uri == uriAllergyReactionManifestation) {
              switch (j) {
                case 0:
                  codeEntry.defaultCoding.code = content;
                  break;
                case 1:
                  // no need to do anything
                  break;
                case 2:
                  codeEntry.defaultCoding.display = content;
                  codeEntry.languageDisplays.en = content;
                  break;
                case 3:
                  codeEntry.languageDisplays.fr = content;
                  break;
                case 4:
                  codeEntry.languageDisplays.de = content;
                  break;
                case 5:
                  codeEntry.languageDisplays.it = content;
                  break;
                default:
                  console.warn('Cannot handle other column, j:' + j);
                  break;
              }
              // no translation for romantsch
              codeEntry.languageDisplays.rm = '?';
            } else if (uri == uriTypeCodes || uri == uriClassCodes) {
              switch (j) {
                case 0:
                  codeEntry.defaultCoding.code = content;
                  break;
                case 1:
                  codeEntry.languageDisplays.de = content;
                  break;
                case 2:
                  codeEntry.defaultCoding.display = content;
                  codeEntry.languageDisplays.en = content;
                  break;
                case 3:
                  codeEntry.languageDisplays.fr = content;
                  break;
                case 4:
                  codeEntry.languageDisplays.it = content;
                  break;
                case 5:
                  codeEntry.languageDisplays.rm = content;
                  break;
                default:
                  console.warn('Cannot handle other column, j:' + j);
                  break;
              }
            } else if (uri == uriBodySite || uri == uriRouteCodes) {
              switch (j) {
                case 0:
                  codeEntry.code = content;
                  break;
                case 1:
                  codeEntry.display = content;
                  break;
                case 2:
                  // do nothing because its empty
                  return;
                default:
                  console.warn('Cannot handle other column, j:' + j);
                  break;
              }
            }
          });

        codeEntries.push(codeEntry);
      });
    console.log(codeEntries);
    clipboard.writeSync(JSON.stringify(codeEntries));
    if (showTypes) {
      console.log('Following types occured: ', codeTypeEntries);
    }
    console.log('DONE, copied result to clipboard as string.');
  } else if (error) {
    console.error(error);
  } else {
    // if this error occurs, the specified URI is invalid and/or outdated
    console.error('\nERR: the specified URI is invalid');
    console.error('=> ' + uri + '\n');
  }
});

/**
 * Cleans string from weird characters.
 * Replaces ’ with '
 *
 * @param {*} string
 * @returns clean string
 */
function cleanString(string) {
  return string.replace('’', "'");
}

/*
* Returns the URI according to parameter

* Running your command with --class will load class codes
* Running your command with --type will load type codes
* If no parameter is specified, the programm will load class codes
*
* @return: The URI for class or type codes
*/
function getURI(args) {
  // set classCode URI as default
  var uri = uriClassCodes;

  // check if the argument for FR has been specified (optional)
  if (
    args.some((val) => {
      return val === '--class';
    })
  ) {
    uri = uriClassCodes;
  }

  if (
    args.some((val) => {
      return val === '--type';
    })
  ) {
    uri = uriTypeCodes;
  }

  if (
    args.some((val) => {
      return val === '--allergy';
    })
  ) {
    uri = uriAllergyCodes;
  }

  if (
    args.some((val) => {
      return val === '--manifest';
    })
  ) {
    uri = uriAllergyReactionManifestation;
  }

  if (
    args.some((val) => {
      return val === '--substance';
    })
  ) {
    uri = uriAllergyReactionSubstance;
  }

  if (
    args.some((val) => {
      return val === '--bodysite';
    })
  ) {
    uri = uriBodySite;
  }

  if (
    args.some((val) => {
      return val === '--route';
    })
  ) {
    uri = uriRouteCodes;
  }

  return uri;
}
