# metadata.js

_metadata.js_ is a ready-to-use node.js script which crawls the website of HL7 about the Swiss Electronic Patient Record FHIR terms and prints [class](https://fhir.ch/ig/ch-epr-term/ValueSet-DocumentEntry.classCode.html#logical-definition-cld) and [type](http://build.fhir.org/ig/hl7ch/ch-epr-term/ValueSet-DocumentEntry.typeCode.html#logical-definition-cld) codes of DocumentReference in JSON format.

Inspired by [Mensajs](https://github.com/frickerg/mensajs)

## Requirements

- Node.js (v8+)
- _v10.15.1 is recommended_

## Installation

First, open your terminal or git bash in order to clone and install this repository into any directory of your choice:

```
git clone https://gitlab.com/drewjosh/metadatajs.git
cd metadatajs
npm install
```

## Usage

Use following command to load DocumentReference.classCode codes:

```
node metadata.js --class
```

Use following command to load DocumentReference.typeCode codes:

```
node metadata.js --type
```

Use following command to load AllergyIntolerance.code (Swiss version) codes:

```
node metadata.js --allergy
```

Use following command to load AllergyIntolerance.reaction.manifestation (Swiss version) codes:

```
node metadata.js --manifest
```

Use following command to load AllergyIntolerance.reaction.substance (Swiss version) codes:

```
node metadata.js --substance
```

Use following command to load AllergyIntolerance.reaction.localisation (Swiss version) codes (only 1000 codes):

```
node metadata.js --bodysite
```

Use following command to load AllergyIntolerance.reaction.exposureRoute (Swiss version) codes:

```
node metadata.js --route
```

You may then copy the array into your own app.

## Format

The data is stored in following format:

```
{
    defaultCoding: {
      system: 'http://snomed.info/sct',
      code: '721965002',
      display: 'Laboratory Order',
    },
    languageDisplays: {
      en: 'Laboratory Order',
      de: 'Laborauftrag',
      fr: "Mandat d'analyse en laboratoire",
      it: 'Richiesta di analisi di laboratorio',
      rm: 'Incumbensa da labor',
    },
  }
```

_Copyright (c) 2022 Joshua Drewlow_

## Contributors Hall of Fame
